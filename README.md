# 欢迎使用 统计帮 

### 新型冠状病毒疫情企业日报系统 
#### 新冠肺炎-新型冠状病毒（2019-nCoV）感染所致肺炎

![效果图](https://github.com/csmake/tjb/blob/master/src/main/webapp/image/image1.jpg?raw=true "效果图")

## 技术特征
1. HTML5 前端页面设计，可以嵌入微信小程序，网站，微信公众号，APP，钉钉等。
2. jQuery+JavaScript，将会采用vue重构。
3. 数据库：Mongodb4.0
4. Java8.0 Servlet
5. 应用服务器:Tomcat8.0 

![效果图](https://github.com/csmake/tjb/blob/master/src/main/webapp/image/image2.jpg?raw=true "效果图")

## 产品特征

1. 免去微信接龙麻烦，各自分别填报，串联改并联，实时性非常高；
2. 对管辖范围的未填人员一目了然，可以直接短信或电话催报；
3. 可以保存历史记录，自动汇总统计，并可以导出Excel；
4. 可自定义待填信息项和统计项模板；
5. 分级分层应用，支持多级自由扩展；
6. 支持公有云使用和私有化、本地化部署；
7. 对集团化大企业支持力度很强，系统管理自上而下设计，轻松细化到5/6级下属公司。数据采集又采用自下而上，分级把关，层层确认。
8. 管理后台与用户填报前台，都可以在手机上完成，无需准备电脑。

![效果图](https://github.com/csmake/tjb/blob/master/src/main/webapp/image/image3.jpg?raw=true "效果图")

## 扫描小程序体验
![统计帮](https://github.com/csmake/tjb/blob/master/src/main/webapp/image/mp.png?raw=true "统计帮")

## QQ交流群  1048813986
![QQ交流群](https://github.com/csmake/tjb/blob/master/src/main/webapp/image/qq.png?raw=true "QQ交流群")


### 相关文档(含效果图)
使用说明：<https://jingyan.baidu.com/article/75ab0bcb14ef6997874db200.html>

### 技术支持与服务

1. 清华软件创新创业联盟
2. [北京赛思美科技术有限公司](http://www.csmake.com "北京赛思美科技术有限公司")
3. [北京一未网络科技有限公司](http://www.healways.cn/portals/ "北京一未网络科技有限公司")
4. [北京嘉绩信息科技有限公司](http://www.performax.com.cn/ "北京嘉绩信息科技有限公司")
5. 广州宏景软件科技有限公司
6. [北京畅发科技有限公司](http://www.chfatech.com)

### 主要客户(截止2020年2月10日，60多家，私有部署)
1. 中国建筑
2. 中铝国际
3. 北京公交
4. 燕山石化
5. 世纪长河
6. 置信大学
7. 工信部电子四院
...

[发送邮件:rd@csmake.com](mailto:rd@csmake.com)
官方网址:<http://www.csmake.com>


